filetype plugin indent on

syntax enable
color slate
set tabstop=4
set softtabstop=4
set expandtab
set shiftwidth=4
set number
set wildmenu
set showcmd
set showmatch
set incsearch
set hlsearch
set autoindent
set smartindent
set hidden
set noswapfile
set nowrap

"Undo History
if !isdirectory($HOME . "/.vim")
    call mkdir($HOME . "/.vim", "", 0770)
endif
if !isdirectory($HOME . "/.vim/undo")
    call mkdir($HOME . "/.vim/undo", "", 0700)
endif
set undodir=~/.vim/undo
set undofile

" Remap leader to space
nnoremap <SPACE> <Nop>
let mapleader=" "

" Remaps - non-plugin
nnoremap <F5> :%s/\s\+$//g<CR>
nmap <F7> :bp<CR>
nmap <F8> :bn<CR>

map <leader>qo :copen<CR>
map <leader>qq :ccl<CR>
map <leader>qn :cn<CR>
map <leader>qp :cp<CR>
map <leader>qf :cfirst<CR>
map <leader>ql :clast<CR>

map <leader>lo :lopen<CR>
map <leader>lq :lcl<CR>
map <leader>ln :ln<CR>
map <leader>lp :lp<CR>
map <leader>lf :lfirst<CR>
map <leader>ll :llast<CR>

nnoremap j jzz
nnoremap k kzz

" Revealing Stuff
set list
set listchars=tab:▸-,trail:·

" Custom Syntax Highlighting
hi Comment ctermfg=black ctermbg=green

" Custom commands
function! CallInTerm(args)
    let t = term_start(['/bin/bash', '-c', a:args])
    call term_setsize(t, 15, 0)
endfunction

command -nargs=* M call CallInTerm('python3 manage.py <args>')

" Plugin Support
"execute pathogen#infect()
"call pathogen#helptags()

"map <C-n> :NERDTreeToggle<CR>
"nmap <F9> :TagbarToggle<CR>

"" nerd commenter
"let g:NERDCreateDefaultMappings = 1

"let g:airline_theme = 'simple'
"let g:airline#extensions#tabline#enabled = 1
"let g:airline_powerline_fonts = 1

"" LSP-VIM && Async completion
"inoremap <expr> <Tab>   pumvisible() ? "\<C-n>" : "\<Tab>"
"inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"
"inoremap <expr> <cr>    pumvisible() ? asyncomplete#close_popup() : "\<cr>"

"imap <c-@> <Plug>(asyncomplete_force_refresh)

"map <leader>lca :LspCodeAction<CR>
"map <leader>td :LspTypeDefinition<CR>
"map <leader>gd :LspDefinition<CR>
"map <leader>gh :LspDeclaration<CR>
"map <leader>ho :LspHover<CR>
"map <leader>ne :LspNextError<CR>
"map <leader>pe :LspPreviousError<CR>
"map <leader>nw :LspNextWarning<CR>
"map <leader>pw :LspPreviousWarning<CR>
"map <leader>rn :LspRename<CR>
"map <leader>dd :LspDocumentDiagnostics<CR>
