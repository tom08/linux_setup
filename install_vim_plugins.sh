#!/bin/bash

# Install vim plugins.

# vim-misc is a dependancy for several of the following
# plugins.
apt-vim install https://github.com/xolox/vim-misc.git

apt-vim install https://github.com/scrooloose/nerdtree.git
apt-vim install https://github.com/Xuyuanp/nerdtree-git-plugin.git
apt-vim install https://github.com/xolox/vim-easytags.git
apt-vim install https://github.com/vim-syntastic/syntastic.git

