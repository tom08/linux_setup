#!/bin/bash

#FIXME: Untested! only individual commands tested.

# Simple setup script for setting up my linux env (Arch).
# Run from top level of this project.

# Excluding git because I use git to retrieve this file!
sudo pacman -Sy vim tmux ctags curl

if [ "$1" == "dev" ]; then #TODO: clang/clang-format
    sudo pacman -Sy gcc cmake boost cppcheck python-pip
fi

# Overwrite default vimrc and bash_profile if they exist
cp .vimrc ~/.vimrc
cp .bash_profile ~/.bash_profile

echo "" >> ~/.bashrc # Mostly for readability
echo "if [ -f ~/.bash_profile ]; then" >> ~/.bashrc
echo "    source ~/.bash_profile" >> ~/.bashrc
echo "fi" >> ~/.bashrc

cd ~

if [ ! -d ".vim" ]; then
    mkdir .vim
fi

cd ~/.vim
if [ ! -d "bundle" ]; then
    mkdir bundle
fi

cd ~

# Install apt-vim for easy install of vim plugins.
curl -sL https://raw.githubusercontent.com/egalpin/apt-vim/master/install.sh | sh

