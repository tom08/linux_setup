#!/bin/bash

# Simple setup script for setting up my linux env (Ubuntu).
# Run from top level of this project.

python --version

if [ $? != 0 ]; then
    python3 --version
    if [ $? == 0 ]; then
        echo "python3 is installed, but apt-vim will fail. You may need to create a symlink from python3 ==> python for it to work."
        echo "Exiting."
        exit 1
    else
        echo "python is not installed for some reason. This script assumes python has been installed."
        echo "Exiting."
        exit 1
    fi
fi


sudo apt update
sudo apt upgrade
sudo apt autoremove

# Excluding git because I use git to retrieve this file!
sudo apt install vim tmux htop curl universal-ctags python3-distutils

if [ "$1" == "dev" ]; then
    sudo apt install gcc cmake libboost-all-dev cppcheck clang-format
fi

# Overwrite default vimrc and bash_profile if they exist
cp .vimrc ~/.vimrc
cp .bash_profile ~/.bash_profile

echo "" >> ~/.bashrc # Mostly for readability
echo "if [ -f ~/.bash_profile ]; then" >> ~/.bashrc
echo "    source ~/.bash_profile" >> ~/.bashrc
echo "fi" >> ~/.bashrc

cd ~

if [ ! -d ".vim" ]; then
    mkdir .vim
fi

cd ~/.vim
if [ ! -d "bundle" ]; then
    mkdir bundle
fi

cd ~

# Install apt-vim for easy install of vim plugins.
curl -sL https://raw.githubusercontent.com/egalpin/apt-vim/master/install.sh | sh

