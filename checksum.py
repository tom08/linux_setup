#!/usr/bin/python
import argparse
import hashlib

def main():
    parser = argparse.ArgumentParser(description="Script to verify downloaded ISO's against provided hash.")
    parser.add_argument("hash", help="SHA256 hash to check against.")
    parser.add_argument("filename", help="File to verify")
    parser.add_argument("--sha", "-s", help="Sha version to use", choices=("1", "256",), default="256")

    args = parser.parse_args()

    with open(args.filename, "rb") as f:
        if args.sha == "256":
            fhash = hashlib.sha256(f.read()).hexdigest()
        elif args.sha == "1":
            fhash = hashlib.sha1(f.read()).hexdigest()
        else:
            print("Unsupported hash!")
            return 1
        print("Validator: {0}".format(args.hash))
        print("File Hash: {0}".format(fhash))
        if fhash == args.hash:
            print("Success.")
        else:
            print("Fail.")

if __name__ == "__main__":
    main()
