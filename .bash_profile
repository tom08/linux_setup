# Aliases
alias la='ls -all -c -h'
alias grep="grep --line-number --color=always"


# PS1 prettifiers
parse_git_branch() {
    git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ (\1)/'
}

export PS1='\[\033[01;36m\][\@]\[\033[00m\] \[\033[01;32m\]\u@\h\[\033[00m\] \[\033[01;35m\][\#]\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\[\033[36m\]$(parse_git_branch) \[\033[00m\]\n\$ '

